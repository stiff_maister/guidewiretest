import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.summingInt;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

public class Main {

	public static void main(String[] args) {
		String example = "00:00:01,400-234-090\n00:05:01,701-080-080\n00:05:00,400-234-090";

		List<String> matches = findMatches(example);
		List<TimeForCall> timeForCalls = matches.stream()
				.map(logRow -> splitLogInTime(logRow))
				.map(timeString -> createTimeForCall(timeString))
				.collect(toList());
		System.out.println("Times: " + timeForCalls);

//		String longestCallNumber =
		List<Map.Entry<String, Integer>> numbersPerTotalCall = timeForCalls.stream()
				.collect(groupingBy(TimeForCall::getPhoneNumber, summingInt(TimeForCall::getTimeCallInSec)))
				.entrySet()
				.stream()
				.sorted(Comparator.comparing(stringIntegerEntry -> stringIntegerEntry.getValue(), Comparator.reverseOrder()))
				.collect(toList());

		System.out.println("Numbers for total call time: " + numbersPerTotalCall);


		Map.Entry<Integer, List<String>> numbersGroupedByTotalTime = numbersPerTotalCall.stream()
				.collect(groupingBy(Map.Entry::getValue))
				.entrySet()
				.stream()
				.collect(toMap(Map.Entry::getKey, entry -> entry.getValue().stream().map(itemList -> itemList.getKey()).collect(toList())))
				.entrySet()
				.stream()
				.findFirst()
				.get();
		System.out.println("Numbers grouped by the maximum total time: " + numbersGroupedByTotalTime);

		Integer minValuePhoneNr = numbersGroupedByTotalTime.getValue().stream()
				.mapToInt(phoneNumber -> Integer.parseInt(phoneNumber.replaceAll("-", "")))
				.min()
				.getAsInt();

		System.out.println("The phone number with the max total call time and the minimum value is : " + minValuePhoneNr);
		String stringPhoneNr = "" + minValuePhoneNr;
		final String numberToFilter = stringPhoneNr.substring(0, 3) + "-" + stringPhoneNr.substring(3, 6) + "-" + stringPhoneNr.substring(6, 9);

		System.out.println(numberToFilter);

		List<TimeForCall> filteredTimeForCall = timeForCalls.stream()
				.filter(timeForCall -> !timeForCall.getPhoneNumber().equals(numberToFilter))
				.collect(toList());
		System.out.println("Time for call list with all the longest call phone numbers filtered out: " + filteredTimeForCall);

		List<Integer> costsPerCall = filteredTimeForCall.stream()
				.map(timeForCall -> calculatePhoneCallCost(timeForCall))
				.collect(toList());
		System.out.println("Costs per call: " + costsPerCall);

		Integer billCost = costsPerCall.stream()
				.reduce(0, (n, m) -> n + m);

		System.out.println("Total bill cost: " + billCost);
	}


	/**
	 * Method that finds all matches of a log line in a string, each log line has to have the following format
	 * hh:mm:ss,nnn-nnn-nnn.
	 *
	 * @param testString the complete phone log
	 * @return a list of log lines.
	 */
	private static List<String> findMatches(String testString) {
		Matcher matcher = Pattern.compile("[0-9]{2}:[0-9]{2}:[0-9]{2},[0-9]{3}-[0-9]{3}-[0-9]{3}").matcher(testString);
		List<String> matches = new ArrayList<>();
		while (matcher.find()) {
			matches.add(matcher.group());
		}
		return matches;
	}

	/**
	 * Method that splits a logRow with the following format hh:mm:ss,nnn-nnn-nnn and returns the 2 parts.
	 *
	 * @param logRow a string with the following format hh:mm:ss,nnn-nnn-nnn.
	 * @return the 2 sections of the provided string.
	 */
	private static String[] splitLogInTime(String logRow) {
		String[] splitLog = logRow.split(",");
		return splitLog;
	}

	/**
	 * Create a time object from a provided string with the following format hh:mm:ss
	 *
	 * @param splitLogString string representing time with this format hh:mm:ss
	 * @return the modelled TimeForCall object.
	 */
	private static TimeForCall createTimeForCall(String[] splitLogString) {
		String[] splitTimeAndPhone = splitLogString[0].split(":");
		return new TimeForCall(splitTimeAndPhone[0], splitTimeAndPhone[1], splitTimeAndPhone[2], splitLogString[1]);
	}

	/**
	 * This method accepts a TimeForCall object and calculates the phone call cost based on its length.
	 * Calls that are shorter than 5 min cost 3 cents per second.
	 * Calls that are longer than 5 min cost 150 cents per full minute.
	 *
	 * @param callTime the call time object.
	 * @return the cost of the call based on its length calculated in cent.
	 */
	private static int calculatePhoneCallCost(TimeForCall callTime) {
		// call shorter than 5 min
		if (callTime.getHours() == 0 && callTime.getMinutes() < 5) {
			return (callTime.getMinutes() * 60) * 3 + callTime.getSeconds() * 3;
		} else {
			// call longer than 5 min
			int hoursAndMinCost = (callTime.getHours() * 60) * 150 + (callTime.getMinutes() * 150);
			if (callTime.getSeconds() == 0) //new minute didn't start yet
				return hoursAndMinCost;
			else
				return hoursAndMinCost + 150;
		}
	}
}

/**
 * Helper class to model the time.
 */
class TimeForCall {
	private int hours;
	private int minutes;
	private int seconds;
	private String phoneNumber;
	private int timeCallInSec;

	public TimeForCall(String hours, String minutes, String seconds, String phoneNumber) {
		this.hours = Integer.parseInt(hours);
		this.minutes = Integer.parseInt(minutes);
		this.seconds = Integer.parseInt(seconds);
		this.phoneNumber = phoneNumber;
		this.timeCallInSec = this.hours * 60 * 60 + this.minutes * 60 + this.seconds;

		if (this.hours >= 100)
			throw new RuntimeException("Hours cannot exceed 99. Provided value: " + this.hours);
		if (this.minutes >= 60)
			throw new RuntimeException("Minutes cannot exceed 59. Provided value: " + this.minutes);
		if (this.seconds >= 60)
			throw new RuntimeException("Seconds cannot exceed 59. Provided value: " + this.seconds);
	}

	public int getHours() {
		return hours;
	}

	public int getMinutes() {
		return minutes;
	}

	public int getSeconds() {
		return seconds;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public int getTimeCallInSec() {
		return timeCallInSec;
	}

	@Override
	public String toString() {
		final StringBuffer sb = new StringBuffer("TimeForCall{");
		sb.append("hours=").append(hours);
		sb.append(", minutes=").append(minutes);
		sb.append(", seconds=").append(seconds);
		sb.append(", phoneNumber='").append(phoneNumber).append('\'');
		sb.append(", timeCallInSec=").append(timeCallInSec);
		sb.append('}');
		return sb.toString();
	}
}
